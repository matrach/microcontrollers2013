_lcd_dump_hex_byte: ; Gets argument in R17!, destroys R16..R17
    swap    R17
    call    _lcd_dump_hex_nibble
    swap    R17
    ; fall-through
_lcd_dump_hex_nibble:
    mov     R16, R17
    andi    R16, 0xF
    ; If R18 < 10: print 0+, else print A+

    subi    R16, -'0'
    cpi R16, '9'+1
    brlt    _lcd_dump_hex_nibble_print
    subi    R16, -('A'-'0'-10)
_lcd_dump_hex_nibble_print:

    call    lcd_send_data
    ret

lcd_dump_byte:
    push    R17
    push    R18
    push    R19
    ; Loop index
    ldi  R18, 8
    mov  R19, R16
    mov  R17, R16

    lcd_dump_byte_loop:
        ldi     R16, '1'
        sbrs    R17,  7
        ldi     R16, '0'
        call    lcd_send_data

        lsl     R17
        dec     R18
        brne    lcd_dump_byte_loop


    LCD_SEND_DATA ' '
    LCD_SEND_DATA '('
    LCD_SEND_DATA '0'
    LCD_SEND_DATA 'x'
    mov R17, R19
    call    _lcd_dump_hex_byte
    LCD_SEND_DATA ')'


    pop     R19
    pop     R18
    pop     R17
    ret

lcd_print_bcd:
    push    R17
    mov     R17, R16
    ; Assuming it's valid BCD...
    call    _lcd_dump_hex_byte
    pop     R17
    ret

lcd_print_string: ; Pass string address (from cseg) in Z, string should be null-terminated
    lpm     R16, Z+
    tst     R16
    breq    lcd_print_string_exit
    call    lcd_send_data
    rjmp    lcd_print_string

    lcd_print_string_exit:
    ret

; -- not used
lcd_print_separated: ; R16 - separator, X: start_addr, Z: end_addr
    push    R17
    push  YH    
    push    YL

    mov R17, R16
    mov YL, XL
    mov YH, XH

    rjmp    lcd_print_separated_loop

    lcd_print_separated_loop_sep:
        mov R16, R17
        call    lcd_send_data

    lcd_print_separated_loop:
        ld      R16, Y+
        call    lcd_print_bcd

        cp  YL, ZL
        brne    lcd_print_separated_loop_sep
        cp  YH, ZH
        brne    lcd_print_separated_loop_sep

    pop     YL
    pop     YH
    pop R17
    ret
