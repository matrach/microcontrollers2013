i2c_fiddle:
   ; Set PAx as output
    ldi R16, 0xFF
    out     DDRA, R16
    out PORTA, R16

    ;debug
    cbi PORTA, 7


    call    i2c_start_transmission

    ;debug
    cbi PORTA, 0

;   I2C_SEND_BYTE_TO_CLOCK    0x00
    ldi     R16, I2C_CLOCK_HWADDR
    call    i2c_select_destination

    ;debug
    cbi PORTA, 1

   ldi  R16, 0x00
    call    i2c_send_data


    ;debug
    cbi PORTA, 2

;   call    i2c_stop -- repeated start

    call    i2c_restart_transmission


    ;debug
    cbi PORTA, 3

    ldi     R16, I2C_CLOCK_HWADDR
    call    i2c_select_destination_for_rd


    ;debug
    cbi PORTA, 4

    call    i2c_read_data_want_more
    mov R17, R16

    call    i2c_read_data
    mov R18, R16


    ;debug
    cbi PORTA, 5
    call    i2c_stop


;    LCD_SET_DDRAM_ADDR 0x00
;    LCD_PRINT_STRING error_str
    mov R16, R17
    call    lcd_dump_byte

    LCD_SET_DDRAM_ADDR 0x40

    mov R16, R18
    call    lcd_dump_byte

    ret

; ---------------------
; I2C Manual operations
; ---------------------
i2c_start_transmission:
    ; Start in master mode
    ldi     R16, 1 << TWINT | 1 << TWSTA | 1<<TWEN
    out     TWCR, R16

    rcall   i2c_poll_twint
    ldi     R16, MT_START
    rcall   i2c_verify
    ret

i2c_restart_transmission:
    ; Start in master mode
    ldi     R16, 1 << TWINT | 1 << TWSTA | 1<<TWEN
    out     TWCR, R16

    rcall   i2c_poll_twint
    ldi     R16, MT_RESTART
    rcall   i2c_verify
    ret

i2c_select_destination: ; Takes one parameter in R16: dest addr
    ; Send address
    ; ldi   R16, ADDR ; ADDR in R16
    out TWDR, R16
    ldi R16, 1<<TWINT | 1 <<TWEN
    out TWCR, R16

    rcall   i2c_poll_twint
    ; Verify
    ldi     R16, MT_SLA_ACK
    rcall   i2c_verify
    ret

i2c_select_destination_for_rd: ; Takes one parameter in R16: dest addr
    ; Send address
    ; ldi   R16, ADDR ; ADDR in R16
    ori R16, I2C_ADDR_READ
    out TWDR, R16
    ldi R16, 1<<TWINT | 1 <<TWEN
    out TWCR, R16

    rcall   i2c_poll_twint
    ldi     R16, MR_SLA_RD_ACK
    rcall   i2c_verify
    ret

.MACRO I2C_SELECT
    ldi R16, @0
    call i2c_select_destination
.ENDMACRO

i2c_send_data: ; R16 - data byte to send
    ; ldi   R16, DATA
    out TWDR,   R16
    ldi R16, 1<<TWINT | 1<<TWEN
    out TWCR, R16

    rcall   i2c_poll_twint
    ldi     R16, MT_DATA_ACK
    rcall   i2c_verify
    ret

i2c_read_data: ; Leaves received value in R16
        ; ldi   R16, DATA

    ldi R16, 1<<TWINT | 1<<TWEN
    out TWCR, R16

    rcall   i2c_poll_twint
    ldi     R16, MR_DATA_END_ACK
    rcall   i2c_verify

    in      R16, TWDR
    ret

i2c_read_data_want_more: ; Leaves received value in R16
        ; ldi   R16, DATA

    ldi R16, 1<<TWINT | 1<<TWEN | 1 <<TWEA
    out TWCR, R16

    rcall   i2c_poll_twint
    ldi     R16, MR_DATA_MORE_ACK
    rcall   i2c_verify

    in      R16, TWDR
    ret

i2c_stop:
    ldi R16, 1 << TWINT | 1<<TWEN | 1<<TWSTO
    out TWCR, R16
    ret
