increment_current_field:
    push    ZL
    push    ZH
    push    R17

    ; It's interrupt-safe if we're here only within interrupt
    lds     R17, current_upper_limit
    LOADW   Z, current_field
    ld      R16, Z

    ; Test for lower limit
    cp      R16, R17
    brsh    icf_skip
    call    bcd_inc
    st      Z, R16

    icf_skip:
    pop R17
    pop ZH
    pop ZL
    ret

increment_current_day:
    push    ZL
    push    ZH
    push    R17

    lds     R17, date_month
    cpi     R17, 2 ; Test for february
    brne    icd_no_feb
    ; Test for leap year
    lds     R16, date_year
    call    bcd_to_bin
    andi    R16, 0x03
    brne    icd_no_feb
    ldi     R17, 0x0d ; set special position
    icd_no_feb:

    ; Find limit in offset table
    GET_PM_ADDR Z, day_limits
    add     ZL, R17
    lpm     R17, Z

    lds     R16, date_day
    ; Test for upper limit
    cp      R16, R17
    brsh    increment_current_day_skip
    call    bcd_inc
    sts     date_day, R16

    increment_current_day_skip:
    pop R17
    pop ZH
    pop ZL
    ret

decrement_current_field:
    push    ZL
    push    ZH
    push    R17

    lds     R17, current_lower_limit
    LOADW   Z, current_field
    ld      R16, Z

    ; Test for lower limit
    cp      R17, R16
    brsh    dcf_skip
    call    bcd_dec
    st      Z, R16

    dcf_skip:
    pop R17
    pop ZH
    pop ZL
    ret

decrement_current_month:
    lds     R16, date_month
    cpi     R16, 0x03 ; March
    brne    decrement_current_field ; do it normally

    dec     R16
    sts     date_month, R16
    lds     R16, date_day
    cpi     R16, 0x28
    brlt    dcm_done
    ldi     R16, 0x28
    sts     date_day, R16
    rjmp    increment_current_day ; To preserve 29th of Feb...


    dcm_done:
    ret

toggle_alarm:
    lds     R16, alarm_enabled
    cpi     R16, BELL_OFF
    ldi     R16, BELL_ON
    breq    ta_set_on
    ldi     R16, BELL_OFF
    ta_set_on:
    sts     alarm_enabled, R16
    ret


copy_view:
    push    ZL
    push    ZH
    mov     ZL, XL
    mov     ZH, XH

    ; Load address of view
    lpm     XL, Z+
    lpm     XH, Z
    mov     ZL, XL
    mov     ZH, XH
    lsl     ZL
    rol     ZH

    GET_ADDR    X, current_view
    cv_loop:
        lpm R16, Z+
        st  X+, R16
        BRNEWI X, current_view_end, cv_loop


    pop     ZH
    pop     ZL
    ret

select_next_view:
    LOADW   X, view_addr_addr

    cpi     XL, LOW(views_last<<1)
    brne    snv_non_overflow
    cpi     XH, HIGH(views_last<<1)
    brne    snv_non_overflow

    GET_PM_ADDR    X, views_table
    rjmp    snv_update
  snv_non_overflow:
    adiw    XH:XL, 2

  snv_update:
    STOREW  view_addr_addr, X
    rcall   copy_view
    ret

defer_date_lock:
    GET_ADDR    X, enter_date_lock
    ; TODO: it may be already set
    STOREW      i2c_finished_hook, X
    ret

enter_alarm_lock:
    sbr     STATUS_REG, 1 << ALARM_BLOCKED
    rjmp    select_next_view

leave_alarm_lock:
    cbr     STATUS_REG, 1 << ALARM_BLOCKED
enter_date_lock:
    sbrc    STATUS_REG, TWI_RUNNING
    rjmp    defer_date_lock

    sbr     STATUS_REG, 1 << TWI_BLOCKED | 1 << NOSLEEP | 1 << ALARM_BLOCKED
    cbr     STATUS_REG, 1 << UPDATE
    call    select_next_view
    ret

leave_date_lock:
    sbrc    STATUS_REG, TWI_RUNNING
    rjmp    defer_date_lock

    call    copy_to_clock_buffer
    call    async_to_clock
    call    select_next_view
    cbr     STATUS_REG, 1 << TWI_BLOCKED | 1 << ALARM_BLOCKED
    ret


end_alarm_option:
    ldi     R16, BELL_OFF
    sts     alarm_enabled, R16
    GET_PM_ADDR     X, views_table
    STOREW          view_addr_addr, X
    rcall           copy_view
    ret

redraw_lcd:
    push YL
    push YH
    push R17
    ; -- Date
    LCD_SET_DDRAM_ADDR 0x00
    LCD_PRINT_STRING    clear_line

    LCD_SET_DDRAM_ADDR DATE_LCD_POS

    ; Day
    lds     R16, date_day
    call    lcd_print_bcd
    LCD_SEND_DATA '-'

    ; Month
    lds     R16, date_month
    call   lcd_print_bcd
    LCD_SEND_DATA '-'

    ; Year
    ldi     R16, 0x20
    call    lcd_print_bcd
    lds     R16, date_year
    call    lcd_print_bcd


    ; -- Alarm
    LCD_SET_DDRAM_ADDR ALARM_LCD_POS

    ; Hours
    lds     R16, alarm_hours
    call    lcd_print_bcd
    LCD_SEND_DATA ':'

    ; Minutes
    lds     R16, alarm_minutes
    call    lcd_print_bcd

    ; Bell
    lds     R16, alarm_enabled
    call    lcd_send_data

    ; -- Time
    LCD_SET_DDRAM_ADDR TIME_LCD_POS

    ; Hours
    lds     R16, time_hours
    call    lcd_print_bcd
    LCD_SEND_DATA ':'

    ; Minutes
    lds     R16, time_minutes
    call    lcd_print_bcd
    LCD_SEND_DATA ':'

    ; Seconds
    lds     R16, time_seconds
    call    lcd_print_bcd

    ldi     R16, ' '
    sbrs    STATUS_REG, TWI_BLOCKED
    ldi     R16, CLOCK
    call    lcd_send_data

    ; --- Set cursor position
    lds     R17, current_cursor_pos
    cpi     R17, 0xFF
    brne    redraw_lcd_move ; 0xFF is special to hide cursor
    ; Hide cursor
    LCD_SEND_INSTR LCD_DISPLAY_LCD_ON
    rjmp    redraw_lcd_after_cursor

  redraw_lcd_move:
    ; XXX: Test it on hardware
    LCD_SEND_INSTR LCD_DISPLAY_LCD_ON | LCD_DISPLAY_CURSOR | LCD_DISPLAY_BLINK
    ori 	R17, LCD_CMD_SET_DDRAM_ADDR
    mov     R16, R17
	call	lcd_send_instruction
  redraw_lcd_after_cursor:
    pop R17
    pop YH
    pop YL
    ret

