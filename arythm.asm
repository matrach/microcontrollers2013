; Binary Coded Decimal - incrementing (without testing borders)
bcd_inc: ; in R16
    ; if lower nibble is 9 -> then R16+7=h:(9+7)=(h+1):l, and l = 0
    subi     R16, -7
    brhc    bcd_inc_lower ; half-byte carry
    subi    R16, 6  ; It wasn't 9, just so add one
bcd_inc_lower:
    ret

; Binary Coded Decimal - decrementing (without testing borders)
bcd_dec: ; in R16
    push    R17

    mov     R17, R16
    andi    R17, 0x0F
    brne    bcd_dec_lower
    subi    R16, 6 ;Correction 15 -> 9
bcd_dec_lower:
    dec     R16

    pop     R17
    ret

bcd_to_bin:
    push    R17
    mov     R17, R16
    andi    R17, 0x0F
    swap    R16
    andi    R16, 0x0F
    breq    btb_exit
    btb_loop:
       subi     R17, -10
       dec      R16
       brne    btb_loop

    btb_exit:
    mov     R16, R17
    pop     R17
    ret
