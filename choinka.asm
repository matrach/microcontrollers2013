;.include "C:\PROG~6\VMLAB\include\m16def.inc"
.include "m16def.inc"

.EQU VMLAB = 0
; Define here Reset and interrupt vectors, if any

.org 0
reset:
    rjmp start
.org INT0addr
    jmp int0_handler
.org INT2addr
    jmp int2_handler
.org OVF0addr
    jmp timer0_overflow
.org OC0addr
    jmp timer0_compare
.org TWIaddr
    jmp twi_handler

; Program starts here after Reset
;
.org 42


;; wywoływanie funkcji: jedyne modyfikowane rejestry to argumenty, R16, R25 i X (R26:R27)
; OR: R17-R20 and R29-R31 (Y, Z) are guaranteed to be preserved (or modified if arguments)
; R21-R24 are reserved

.include "waiting.asm"

; Psuje X
.MACRO SLEEP_DMS
    ldi XL, LOW(@0)
    ldi XH, HIGH(@0)
    call    waitdms_long
.ENDMACRO

; Psuje R16
.MACRO SLEEP_US
    ldi R16, @0
    call    waitus
.ENDMACRO


.MACRO WMS
    nop       ;
    nop       ;
    nop       ;
    nop       ;
    nop       ;
    nop       ;
    nop       ;
    nop       ;
.ENDMACRO

.MACRO SLEEP_MSH
    ldi     R16, @0 / 10
    call    waitcs
.ENDMACRO

.MACRO SLEEP_MSL
    ldi     R16, @0 * 10
    call    waitdms
.ENDMACRO

; destroys Z
.MACRO LCD_PRINT_STRING
    ldi     ZL, LOW(@0<<1)
    ldi     ZH, HIGH(@0<<1)
    call    lcd_print_string
.ENDMACRO

.MACRO POPW
    pop @0H
    pop @0L
.ENDMACRO

.MACRO PUSHW
    push @0L
    push @0H
.ENDMACRO

.MACRO GET_ADDR
    ldi @0L, LOW(@1)
    ldi @0H, HIGH(@1)
.ENDMACRO

.MACRO GET_PM_ADDR
    ldi @0L, LOW(@1<<1)
    ldi @0H, HIGH(@1<<1)
.ENDMACRO


.MACRO STOREC
    ldi XL, LOW(@0)
    ldi XH, HIGH(@0)
    ldi R16, @1
    st      X, R16
.ENDMACRO

.MACRO STORE
    sts      @0, @1
.ENDMACRO

.MACRO STOREW
    sts @0, @1L
    sts @0+1, @1H
.ENDMACRO

.MACRO LOAD
    ldi XL, LOW(@1)
    ldi XH, HIGH(@1)
    ld  @0, X
.ENDMACRO

.MACRO LOADW
    lds @0L, @1
    lds @0H, @1+1
.ENDMACRO

.MACRO SAVE_VOLATILE
    push    R16
    in      R16, SREG
    push    R16
    push    XH
    push    XL
.ENDMACRO

.MACRO RESTORE_VOLATILE
    pop     XL
    pop     XH
    pop     R16
    out     SREG, R16
    pop     R16
.ENDMACRO

.MACRO BRNEWI
    cpi     @0L, LOW(@1)
    brne    @2
    cpi     @0H, HIGH(@1)
    brne    @2
.ENDMACRO

.MACRO BRNEW
    cp     @0L, @1L
    brne    @2
    cp     @0H, @1H
    brne    @2
.ENDMACRO

.include "lcd.asm"
.include "lcd_utils.asm"
.include "i2c.asm"
.include "arythm.asm"
.include "play_file.asm"

.EQU I2C_CLOCK_HWADDR = 0b11010000
.EQU I2C_SQWE         = 4


.EQU    KEYS_DDR      = DDRB
.EQU    KEYS_PORT     = PORTB
.EQU    KEYS_PIN      = PINB
.EQU    SELECT_KEY_PD = PB5
.EQU    UP_KEY_PD     = PB6
.EQU    DOWN_KEY_PD   = PB7
.EQU    KEYS_OR_PIN   = PINB
.EQU    KEYS_OR_PD    = PB2


.def    STATUS_REG     = R24
.EQU    TWI_RUNNING    = 0
.EQU    TWI_BLOCKED    = 1
.EQU    BUTTON_RUNNING = 2
.EQU    UPDATE         = 3
.EQU    NOSLEEP        = 4
.EQU    ALARM_BLOCKED  = 5

.EQU	  BELL_REPEATS	  = 5

.org (PC | 0xF) + 1
int0_handler:
    SAVE_VOLATILE

    mov     R16, STATUS_REG
    andi    R16, 1 << TWI_RUNNING | 1 << TWI_BLOCKED
    brne    int0_skip

    LOADW   Z, i2c_current_recipe
    BRNEWI  Z, 0x0000, int0_skip

    call    async_from_clock

    int0_skip:
    RESTORE_VOLATILE
    reti


int2_handler:
    SAVE_VOLATILE
    ; rcall   handle_select_button

    ; Start timer0 with 1/1024 prescaler
    ; And with CTC (should be set to 128 -> Giving 16Hz
    ldi     R16, 1 << CS02 | 1 << CS00 | 1 << WGM01
    out     TCCR0, R16

    sbr     STATUS_REG, 1 << BUTTON_RUNNING
    RESTORE_VOLATILE
    reti

timer0_compare:
timer0_overflow:
    SAVE_VOLATILE
    rcall   timer0_reset


    ; Skip if button not pressed
    sbis    KEYS_PIN, SELECT_KEY_PD
    rcall   handle_select_button
    sbis    KEYS_PIN, UP_KEY_PD
    rcall   handle_up_button
    sbis    KEYS_PIN, DOWN_KEY_PD
    rcall   handle_down_button

    timer0_ov_skip:
    cbr     STATUS_REG, 1 << BUTTON_RUNNING
    ; Reset TIMER0
    RESTORE_VOLATILE
    reti

timer0_reset:
    ldi     R16, 0
    out     TCNT0, R16
    out     TCCR0, R16
    ret

twi_handler:
    PUSHW   Z
    PUSHW   Y
    call    i2c_run_phase
    LOADW   Y, i2c_current_recipe_end
    LOADW   Z, i2c_current_recipe
    BRNEW   Z, Y, twih_has_next

    ; TWI finished
    ldi     R16, 0x00
    sts     i2c_current_recipe,   R16
    sts     i2c_current_recipe+1, R16

    cbr     STATUS_REG, 1 << TWI_RUNNING
    sbr     STATUS_REG, 1 << UPDATE

    GET_ADDR   X, i2c_finished_hook
    ldi     R16, 0x00
    sts     i2c_finished_hook,   R16
    sts     i2c_finished_hook+1, R16
    rcall   call_X


    twih_has_next:
    POPW    Y
    POPW    Z
    reti

call_X:
    push    ZL
    push    ZH
    ld      ZL, X+
    ld      ZH, X
    adiw    ZH:ZL, 0
    breq    call_x_skip ; Skip if 0x0000
    icall
  call_x_skip:
    pop ZH
    pop ZL
    ret

handle_select_button:
    GET_ADDR    X, button_select_handle
    call    call_X
    ret

handle_up_button:
    GET_ADDR    X, button_up_handle
    call    call_X
    ret

handle_down_button:
    GET_ADDR    X, button_down_handle
    call    call_X
    ret

.include "views.asm"
.EQU    CBUFFER_SIZE = 7

copy_to_clock_buffer:
    push    ZL
    push    ZH

    GET_ADDR    Z, local_buffer_start
    GET_ADDR    X, clock_buffer
    ld      R16, Z+ ; seconds -> keep CH to 0
    st      X+, R16
    ld      R16, Z+ ; minutes
    st      X+, R16
    ld      R16, Z+ ; hours -> keep 24 to 0 -> 24h mode
    st      X+, R16
    ld      R16, Z+ ; week
    st      X+, R16
    ld      R16, Z+ ; day
    st      X+, R16
    ld      R16, Z+ ; month
    st      X+, R16
    ld      R16, Z+ ; year
    st      X+, R16

    pop     ZH
    pop     ZL
    ret


copy_from_clock_buffer:
    push    ZL
    push    ZH

    GET_ADDR    X, local_buffer_start
    GET_ADDR    Z, clock_buffer
    ld      R16, Z+ ; seconds -> clear CH flag, if set
    andi    R16, 0x7f
    st      X+, R16
    ld      R16, Z+ ; minutes
    st      X+, R16
    ld      R16, Z+ ; hours -> clear 12h mode if set
    andi    R16, 0x3f
    st      X+, R16
    ld      R16, Z+ ; week
    st      X+, R16
    ld      R16, Z+ ; day
    st      X+, R16
    ld      R16, Z+ ; month
    st      X+, R16
    ld      R16, Z+ ; year
    st      X+, R16

    pop     ZH
    pop     ZL
    ret

keys_init:
    ; Buttons input
    in  R16, KEYS_DDR
    sbr R16, 1 << SELECT_KEY_PD | 1 << UP_KEY_PD | 1 << DOWN_KEY_PD
    out KEYS_DDR, R16
    ; resistors
    in  R16, KEYS_PORT
    sbr R16, (1 << SELECT_KEY_PD | 1 << UP_KEY_PD | 1 << DOWN_KEY_PD)
    out KEYS_PORT, R16

    ; Initialize INT2 for buttons checking
    cbi DDRB, PB2
    sbi PORTB, PB2 ; rezystor podciagajacy

    in  r16, MCUCSR
    cbr R16, 1 << ISC2
    out MCUCSR, r16

    in  R16, GICR
    sbr R16, 1 << INT2
    out GICR, R16

    ldi R16, 1 << INTF2
    out GIFR, R16

    ; Using Timer0 for re-checking
    ; Normal
    ; 1/1024 prescaler + 96CTC -> 8Mhz -> 2^23 / 2^16 = 64 Hz
    ; do not use OC0/PB3
    call    timer0_reset

;    ldi R16, 128
    ldi R16, 255
    out OCR0, R16

    ; Use compare interrupt
    in  R16, TIMSK
    sbr R16, 1 << OCIE0
    cbr R16, 1 << TOIE0
    out TIMSK, R16

    ; Reset interrupt flags
    ldi R16, 1 << OCF0 | 1 << TOV0
    out TIFR, R16

    ret

.include "i2c_test.asm"

clock_init:
    ; Initialize INT0
    cbi DDRD, PD2
    sbi PORTD, PD2 ; rezystor podciagajacy

    in  R16, MCUCR
    sbr R16, 1 << ISC01 ; zbocze opadajace
    cbr R16, 1 << ISC00
    out MCUCR, R16

    in  R16, GICR
    sbr R16, 1 << INT0
    out GICR, R16

    ldi R16, 1 << INTF0
    out GIFR, R16 ; Reset INT0 state


    ldi     R16, I2C_CLOCK_HWADDR
    sts     i2c_clock_addr, R16
    ori     R16, I2C_ADDR_READ
    sts     i2c_clock_read_addr, R16

    ldi     R16, 1 << I2C_SQWE
    sts     clock_command_byte, R16

    call    i2c_init
    call    sync_from_clock
    call    sync_to_clock
    ret

sync_from_clock:
    GET_PM_ADDR X, i2c_read_clock
    STOREW      i2c_current_recipe, X
    GET_PM_ADDR X, i2c_read_clock_end
    STOREW      i2c_current_recipe_end, X

    call    i2c_run_synchronous
    call    copy_from_clock_buffer
    ldi     R16, 0x00
    sts     i2c_current_recipe,   R16
    sts     i2c_current_recipe+1, R16
    ret

async_from_clock:
    GET_PM_ADDR X, i2c_read_clock
    STOREW      i2c_current_recipe, X
    GET_PM_ADDR X, i2c_read_clock_end
    STOREW      i2c_current_recipe_end, X

    sbr     STATUS_REG, 1 << TWI_RUNNING
    call    i2c_start_async
    ret

sync_to_clock:
    GET_PM_ADDR X, i2c_write_clock
    STOREW      i2c_current_recipe, X
    GET_PM_ADDR X, i2c_write_clock_end
    STOREW      i2c_current_recipe_end, X

    call    copy_to_clock_buffer
    call    i2c_run_synchronous
    ldi     R16, 0x00
    sts     i2c_current_recipe,   R16
    sts     i2c_current_recipe+1, R16
    ret

async_to_clock:
    GET_PM_ADDR X, i2c_write_clock
    STOREW      i2c_current_recipe, X
    GET_PM_ADDR X, i2c_write_clock_end
    STOREW      i2c_current_recipe_end, X

    sbr     STATUS_REG, 1 << TWI_RUNNING
    call    i2c_start_async
    ret


sleep_init:
	 in      R16, MCUCR
	 cbr     R16, 1 << SM2 | 1 << SM1 | 1 << SM0
	 sbr     R16, 1 << SE
	 out     MCUCR, R16
	 ret


current_tests:
    call	player_init
    true:
    	call process_file
    	rjmp true
    call	player_stop
    jmp     dead
    ret

input_tests:
    push R17
    ldi R16, 0xFF
    out DDRA, R16
    out PORTA, R16
    ldi R16, 0x00
    out DDRB, R16
    ldi R16, 0xFF
    out PORTB, R16

    it_loop:
        in  R16, PINB
        ori R16, 0xb00011011

        in  R17, PORTA
        ori R17, 0xb11100100
        and R16, R17

        out PORTA, R16
        ;jmp it_loop
    pop R17
    ret

alarm_init:
    ldi     R16, BELL_OFF
    sts     alarm_enabled, R16
    ret

play_alarm:
    push    R17
    cli
    GET_PM_ADDR     X, alarm_view_addr_addr
    STOREW          view_addr_addr, X
    rcall           copy_view
    sei
    LCD_SET_DDRAM_ADDR  0x00
    LCD_PRINT_STRING    alarm_line1
    LCD_SET_DDRAM_ADDR  0x40
    LCD_PRINT_STRING    alarm_line2


	call		player_init
	ldi	R17, BELL_REPEATS
    play_alarm_loop:
;        call    process_file
        sleep
        dec     R17
        breq    play_alarm_end
        lds     R16, alarm_enabled
        cpi     R16, BELL_OFF
        breq    play_alarm_end
        sbrs    STATUS_REG, ALARM_BLOCKED
	    rjmp    play_alarm_loop

    play_alarm_end:
    call		player_stop
    pop     R17
    ret

start:
    LDI R16, HIGH(RAMEND)
    OUT SPH, R16
    LDI R16, LOW(RAMEND)
    OUT SPL, R16


   ; Set PAx as output
    ldi R16, 0xFF
    out DDRA, R16
    out PORTA, R16

    ; Clear the memory
    ldi R16, 0
    GET_ADDR    X, mem_start

    mem_clear:
        st      X+, R16
        BRNEWI  X, mem_end, mem_clear



    ; Init fields (for testing and good look mainly)
    GET_ADDR    X, fields_start
    GET_PM_ADDR Z, initial_fields
    initial_fields_loop:
        lpm     R16, Z+
        st      X+, R16
        BRNEWI  X, fields_end, initial_fields_loop

    GET_PM_ADDR X, views_table
    STOREW  view_addr_addr, X
    call    copy_view

;    call    current_tests

    clr		STATUS_REG

    call    lcd_init
    call    keys_init
    call    clock_init
    call    alarm_init
    call    sleep_init

;   jmp dead

.IF VMLAB == 0
    call copy_charmap
.ELSE
    nop
.ENDIF

    call redraw_lcd

    sei ; XXXX <- ENABLE INTERRUPTS

forever:
    ; TODO: IF !TWI_RUNNING & !BUTTON_RUNNING: set deep-sleep-mode
    sbrs    STATUS_REG, NOSLEEP
    sleep
    cbr     STATUS_REG, 1 << NOSLEEP
	 call  input_tests
	

    ; Skip updating if flag not set
    sbrs    STATUS_REG, UPDATE
    rjmp    no_update
    cli
    cbr     STATUS_REG, 1 << UPDATE
    call    copy_from_clock_buffer
    sei
    no_update:

    sbrc    STATUS_REG, ALARM_BLOCKED
    rjmp    dont_alarm
    call    test_for_alarm

    dont_alarm:
    call    redraw_lcd
    rjmp forever

test_for_alarm:
    push    R17
    lds     R16, alarm_enabled
    cpi		R16, BELL_OFF
    breq    tfa_no_alarm

    ; Run alarm only with seconds 0 or 1
    lds     R16, time_seconds
    cpi     R16, 0x02
    brge    tfa_no_alarm

    lds     R16, alarm_hours
    lds     R17, time_hours
    cp      R16, R17
    brne    tfa_no_alarm

    lds     R16, alarm_minutes
    lds     R17, time_minutes
    cp      R16, R17
    brne    tfa_no_alarm

    call    play_alarm

    tfa_no_alarm:
    pop     R17
    ret

i2c_ERROR:
    LCD_SET_DDRAM_ADDR 0x00
    in      R16, TWCR
    andi    R16, 0xF8
    call    lcd_dump_byte


    LCD_SET_DDRAM_ADDR 0x40
    LCD_PRINT_STRING error_str

dead:
    nop
    rjmp dead



error_str: .db "CLOCK ERROR",0
alarm_line1: .db "      WAKE      "
alarm_line2: .db "       UP       "
clear_line: .db "                "

.dseg
.org 0x80
mem_start:
    alarm_enabled:
        .BYTE 1

    fields_start:
    alarm_minutes: .BYTE 1
    alarm_hours: .BYTE 1
    alarm_end:
    local_buffer_start:
    time_seconds: .BYTE 1
    time_minutes: .BYTE 1
    time_hours: .BYTE 1
    time_end:
    date_week: .BYTE 1
    date_day: .BYTE 1
    date_month: .BYTE 1
    date_year: .BYTE 1
    date_end:
    local_buffer_end:
    fields_end:
        .BYTE 1

    ; Buffer for interacting with timer
    clock_buffer: .BYTE 7
    clock_buffer_end:
    clock_command_byte:
        .BYTE 1 ; aligning bit

    ; Global memory
    ; Synchronization/locks (or just disable interrupts)
    always_0:
        .BYTE 2

    i2c_clock_addr:
        .BYTE 1
    i2c_clock_read_addr:
        .BYTE 1

    i2c_current_recipe:
        .BYTE 2
    i2c_current_recipe_end:
        .BYTE 2

    i2c_finished_hook:
        .BYTE 2


    ; Current view-methods
    ; When changing view, view data is copied here to allow easy method invocation
    view_addr_addr:
        .BYTE 2

    current_view:
    button_select_handle: .BYTE 2
    button_up_handle: .BYTE 2
    button_down_handle: .BYTE 2
    current_field:
        .BYTE 2
    current_cursor_pos:
        .BYTE 1
    current_upper_limit:
        .BYTE 1
    current_lower_limit:
        .BYTE 1
    current_view_data: .BYTE 18
    current_view_end:
mem_end:


; Align the address, so that we can load only HIGH byte, and put offset in LOW
; Or at least 8-bit adding to LOW is safe
.cseg
.EQU    DATE_LCD_POS    = 0x06
.EQU    TIME_LCD_POS    = 0x47
.EQU    ALARM_LCD_POS   = 0x40

idle_view:
    .dw enter_alarm_lock
    .dw toggle_alarm
    .dw toggle_alarm
    .dw 0x0000
    .dw 0xFF,0,0,0,0,0

alarm_h_view:
    .dw select_next_view
    .dw increment_current_field
    .dw decrement_current_field
    .dw alarm_hours
    .db ALARM_LCD_POS+1, 0x23, 0x0, 0
    .db 0,0

alarm_m_view:
    .dw leave_alarm_lock
    .dw increment_current_field
    .dw decrement_current_field
    .dw alarm_minutes
    .db ALARM_LCD_POS+4, 0x59, 0x0, 0
    .db 0,0


time_h_view:
    .dw select_next_view
    .dw increment_current_field
    .dw decrement_current_field
    .dw time_hours
    .db TIME_LCD_POS+1, 0x23, 0x0, 0
    .db 0,0

time_m_view:
    .dw select_next_view
    .dw increment_current_field
    .dw decrement_current_field
    .dw time_minutes
    .db TIME_LCD_POS+4, 0x59, 0x00, 0
    .db 0,0

time_s_view:
    .dw select_next_view
    .dw increment_current_field
    .dw decrement_current_field
    .dw time_seconds
    .db TIME_LCD_POS+7, 0x59, 0x00, 0
    .db 0,0

date_d_view:
    .dw select_next_view
    .dw increment_current_day
    .dw decrement_current_field
    .dw date_day
    .db DATE_LCD_POS+1, 0x31, 0x01, 0
    .db 0,0

date_m_view:
    .dw select_next_view
    .dw increment_current_field
    .dw decrement_current_month
    .dw date_month
    .db DATE_LCD_POS+4, 0x12, 0x1, 0
    .db 0,0

date_y_view:
    .dw leave_date_lock
    .dw increment_current_field
    .dw decrement_current_field
    .dw date_year
    .db DATE_LCD_POS+9, 0x99, 0x0, 0
    .db 0,0

alarm_view:
    .dw end_alarm_option
    .dw end_alarm_option
    .dw end_alarm_option
    .dw 0x0000
    .db 0xff, 0x00, 0x0, 0
    .db 0,0

; Views table (call-table-table)
views_table:
    .dw idle_view
    .dw alarm_h_view
    .dw alarm_m_view
    .dw time_h_view
    .dw time_m_view
    .dw time_s_view
    .dw date_d_view
    .dw date_m_view
    views_last:
    .dw date_y_view
view_table_end:
alarm_view_addr_addr:
    .dw alarm_view


; I2C recipes
; One recipe has structure:
; .db EXPECTED_TWSR, WHAT_TO_DO_WITH_TWDR (0-skip, 1 <-put on bus, 2->save)
; .dw TWDR_PUT/GET_ADDR
; .db TWCR, 0

i2c_read_clock: ; implicit start
    ; select dest
    .db MT_START, I2C_PUT
    .dw i2c_clock_addr
    .db 1<<TWINT | 1 <<TWEN, 0
    ; write start addr (send 0)
    .db MT_SLA_ACK, I2C_PUT
    .dw always_0
    .db 1<<TWINT | 1 <<TWEN, 0
    ; restart
    .db MT_DATA_ACK, I2C_IGNORE
    .dw 0x0000
    .db 1<<TWINT | 1 << TWEN | 1 << TWSTA, 0
    ; send destination again, but for read
    .db MT_RESTART, I2C_PUT
    .dw i2c_clock_read_addr
    .db 1<<TWINT | 1 <<TWEN, 0
    ; Receive ACK, and prepare to ACK
    .db MR_SLA_RD_ACK, I2C_IGNORE
    .dw 0x0000
    .db 1<<TWINT | 1 <<TWEN | 1<<TWEA
    ; 1 -> read first byte, want more
    .db MR_DATA_MORE_ACK, I2C_SAVE
    .dw clock_buffer
    .db 1<<TWINT | 1<<TWEN | 1<<TWEA, 0
    ; 2 (read bytes 2-7)
    .db MR_DATA_MORE_ACK, I2C_SAVE
    .dw clock_buffer + 1
    .db 1<<TWINT | 1<<TWEN | 1<<TWEA, 0
    ; 3
    .db MR_DATA_MORE_ACK, I2C_SAVE
    .dw clock_buffer + 2
    .db 1<<TWINT | 1<<TWEN | 1<<TWEA, 0
    ; 4
    .db MR_DATA_MORE_ACK, I2C_SAVE
    .dw clock_buffer + 3
    .db 1<<TWINT | 1<<TWEN | 1<<TWEA, 0
    ; 5
    .db MR_DATA_MORE_ACK, I2C_SAVE
    .dw clock_buffer + 4
    .db 1<<TWINT | 1<<TWEN | 1<<TWEA, 0
    ; 6 -> say that next is last
    .db MR_DATA_MORE_ACK, I2C_SAVE
    .dw clock_buffer + 5
    .db 1<<TWINT | 1<<TWEN, 0
    ; 7 -> (nack), read last byte, stop
    .db MR_DATA_END_ACK, I2C_SAVE
    .dw clock_buffer + 6
    .db 1<<TWINT | 1<<TWEN | 1<<TWSTO, 0
i2c_read_clock_end:


i2c_write_clock: ; implicit start
    ; select dest
    .db MT_START, I2C_PUT
    .dw i2c_clock_addr
    .db 1<<TWINT | 1 <<TWEN, 0
    ; write start addr (send 0)
    .db MT_SLA_ACK, I2C_PUT
    .dw always_0
    .db 1<<TWINT | 1 <<TWEN, 0
    ; 1 -> write first byte
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_buffer
    .db 1<<TWINT | 1<<TWEN, 0
    ; 2
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_buffer + 1
    .db 1<<TWINT | 1<<TWEN, 0
    ; 3
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_buffer + 2
    .db 1<<TWINT | 1<<TWEN, 0
    ; 4
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_buffer + 3
    .db 1<<TWINT | 1<<TWEN, 0
    ; 5
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_buffer + 4
    .db 1<<TWINT | 1<<TWEN, 0
    ; 6
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_buffer + 5
    .db 1<<TWINT | 1<<TWEN, 0
    ; 7 -> send last byte
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_buffer + 6
    .db 1<<TWINT | 1<<TWEN, 0
    ; 8 -> send status
    .db MT_DATA_ACK, I2C_PUT
    .dw clock_command_byte
    .db 1<<TWINT | 1<<TWEN, 0
    ; stop
    .db MT_DATA_ACK, I2C_IGNORE
    .dw 0x0000
    .db 1<<TWINT | 1<<TWEN | 1<<TWSTO, 0
i2c_write_clock_end:

.org (PC | 0x7F) + 1
; !! VALUES USED IN FIELDS ARE BCD !!
day_limits: ; offset in BCD, february without leap-year, leap-february (| 0xc)
    .db 0x00, 0x31
    .db 0x28, 0x31
    .db 0x30, 0x31
    .db 0x30, 0x31
    .db 0x31, 0x30
    .db 0x00, 0x00
    .db 0x00, 0x29
    .db 0x00, 0x00
    .db 0x31, 0x30
    .db 0x31, 0x00
initial_fields:
    .db 0x00, 0x00, 0x00, 0x59, 0x23, 0, 0x30, 0x03, 0x12, 0, 0, 0

.include "charmap.asm"

FILE:
.include "sound_data.asm"
FILE_END:











