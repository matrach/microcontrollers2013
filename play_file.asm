process_file:
	; Z iterates over file
    GET_PM_ADDR Z, FILE
	
	; non empty file
	process_loop:
		lpm	R16, Z+
		out	OCR2, R16
	
		; sleep for 1/8000 second
		ldi R16, 125
		call	waitus
		
		; check if end
        BRNEWI  Z, (FILE_END<<1), process_loop
	
	ret
	
; this will always use PD7 as output
player_init:
    sbi DDRD, PD7
    ; Using Timer2 for sound
	; Start with 50% fill
	ldi R16, 127
	out OCR2, r16

	; tryb PWM poprawny fazowo
	; OC2 jest 1 na [0, ocr2]
	; preskaler 64
	;ldi R16, 1 << WGM20 | 1 << COM20 | 1 << CS22
	ldi R16, 1 << COM20 | 1 << CS22
	out TCCR2, R16	
	ret

player_stop:
	ldi R16, 0
	out TCCR2, R16	
	ret

