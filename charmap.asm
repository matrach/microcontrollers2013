.IF VMLAB == 0
.EQU    BELL_OFF = 0x00
.EQU    BELL_ON  = 0x01
.EQU    CLOCK    = 0x02
.ELSE
.EQU    BELL_OFF = 'D'
.EQU    BELL_ON  = 'E'
.EQU    CLOCK    = 'C'
.ENDIF

copy_charmap:
	push R17
	ldi	R16, LCD_CMD_SET_CGRAM_ADDR
	call	lcd_send_instruction
	SLEEP_US 80
	
	; LOOP ON CHARMAP
	ldi	ZL, LOW(CHARMAP<<1)
	ldi	ZH, HIGH(CHARMAP<<1)
	copy_charmap_loop:
		lpm	R16, Z+
		lpm	R17, Z+ ; Dummy 0-byte
		call	lcd_send_data
		cpi	ZL, LOW(CHARMAP_END<<1)
		brne	copy_charmap_loop
		cpi	ZH, HIGH(CHARMAP_END<<1)
		brne	copy_charmap_loop
	pop R17

    LCD_SET_DDRAM_ADDR 0x00
	ret

CHARMAP:
; IT WILL BE IN SEPARATE WORDS
;BELL OFF
.db 0b00000100
.db 0b00001110
.db 0b00001110
.db 0b00001110
.db 0b00011111
.db 0b00000000
.db 0b00000000
.db 0b00000000
; BELL
.db 0b00000100
.db 0b00001110
.db 0b00001110
.db 0b00001110
.db 0b00011111
.db 0b00000000
.db 0b00000100
.db 0b00000000
;CLOCK
.db 0b00000000
.db 0b00001110
.db 0b00010101
.db 0b00010111
.db 0b00010001
.db 0b00001110
.db 0b00000000
.db 0b00000000
;NOTE
.db 0x2,0x3,0x2,0x6,0xe,0x1e,0xc,0x0
CHARMAP_END:

