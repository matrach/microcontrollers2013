; I2C Library
; Assumed external labels
; * i2c_current_recipe in 2bytes in DSEG
; * i2c_ERROR - code handling error

.EQU MT_START = 0x08
.EQU MT_RESTART = 0x10
.EQU MT_SLA_ACK = 0x18
.EQU MT_DATA_ACK = 0x28

.EQU MR_SLA_RD_ACK = 0x40
.EQU MR_DATA_MORE_ACK = 0x50
.EQU MR_DATA_END_ACK = 0x58
.EQU I2C_ADDR_READ = 1

.EQU I2C_IGNORE = 0
.EQU I2C_PUT    = 1
.EQU I2C_SAVE   = 2


;.MACRO I2C_VERIFY_TWSR
;    ldi     R16, @0
;    call   i2c_verify
;.ENDMACRO

i2c_init:
    ; TWINT - reset operation
    ; TWSTA - master mode
    ; TWEN - connect PC0, PC1 to I2C
    ldi     R16, 32
    out     TWBR, R16 ; 16  + 32*2*1 = 80 -> 100K
    ldi     R16, 1 << TWINT | 1<<TWEN
    out     TWCR, R16

    ; rezystory podciagajace
    cbi     DDRC, PC0
    cbi     DDRC, PC1
    sbi     PORTC, PC0
    sbi     PORTC, PC1
    ret

i2c_run_synchronous: ; Current phase addr in i2c_current_recipe
                     ; End in i2c_current_recipe_end
    PUSHW   Z
    PUSHW   Y
    LOADW   Y, i2c_current_recipe_end

    rcall   i2c_start

    i2crs_next_ph:
        rcall   i2c_poll_twint
        rcall   i2c_run_phase
        LOADW   Z, i2c_current_recipe
        BRNEW   Z, Y, i2crs_next_ph

    POPW   Y
    POPW   Z
    ret


i2c_poll_twint:
    in      R16, TWCR
    sbrs    R16, TWINT
    rjmp    i2c_poll_twint
    ret

i2c_verify: ;
    in      R25, TWSR
    andi    R25, 0xF8
    cp      R25, R16
    brne    i2c_ERROR_long_jmp
    ret

i2c_ERROR_long_jmp:
    jmp i2c_ERROR

i2c_run_phase: ; Current phase addr in i2c_current_recipe
    ; .db EXPECTED_TWSR, WHAT_TO_DO_WITH_TWDR (0-skip, 1 <-put on bus, 2->save)  
    ; .dw TWDR_PUT/GET_ADDR
    ; .db TWCR, 0
    PUSHW   Z
    push    R17

    LOADW   Z, i2c_current_recipe
    lpm     R16, Z+
    rcall   i2c_verify

    lpm     R16, Z+ ; what to do
    lpm     XL, Z+ ; address
    lpm     XH, Z+

    cpi     R16, I2C_IGNORE
    breq    i2crp_continue

    cpi     R16, I2C_SAVE
    brne    i2crp_put
    in      R16, TWDR
    st      X, R16
    rjmp    i2crp_continue

    i2crp_put:
    ld      R16, X
    out     TWDR, R16

    i2crp_continue:
    ; TODO: simplify last-bit-preservation
    in      R17, TWCR
    andi    R17, 0x1
    lpm     R16, Z+ ; control
    or      R16, R17
    out     TWCR, R16

    adiw    ZH:ZL, 1 ; aligning byte
    STOREW  i2c_current_recipe, Z
    pop     R17
    POPW    Z
    ret
    

i2c_start:
    ldi     R16, 1 << TWINT | 1 << TWSTA | 1<<TWEN
    out     TWCR, R16
    ret

i2c_start_async:
    ldi     R16, 1 << TWINT | 1 << TWSTA | 1<<TWEN | 1 << TWIE
    out     TWCR, R16
    ret

