waitus: ; argument in R16, 128 > R16 > 1
    nop
    nop
    nop
    dec R16     ; 5 (substract one for auxliary ops)
waitus_loop:
    dec R16      ; 1
    nop ; 2
    nop   ; 3
    nop   ; 4
    nop   ; 5
    nop   ; 6
    brne waitus_loop  ; 8 cycles on jump, 7 on ok   -> loop takes 1us
    ret            ; +4-1 = 8 cycles

waitdms: ; argument in R16 > 1  (10^-4 seconds - deci-mili-seconds)
    push R17   ; +2
    mov R17, R16 ; 3

    dec R17 ; 4
waitdms_loop:
    ldi R16, 99 ;  sleep for 99us, 1
    call waitus   ; +99us, 5
    dec R17   ; 6
    brne waitdms_loop  ; 8 cycles on jump, 7 on ok   -> loop takes 100us

    ; +4 cycles (4 - 1)
    ldi R16, 98 ; 100ms - 16 cycles +1
    call     waitus ; 4 cycles
    nop ; +1
    nop     ; + 1
    pop R17 ; + 2
    ret      ; + 4 cycles
    ; 98us + 2us (16 cycles)
    ; exactly (R16 * 10^-4 s)

waitcs: ; argument in R16 > 1 (centiseconds ;) *10^-2)
    push R17   ; +2 cycles
    mov R17, R16 ; 3
waitcs_loop:
    ldi R16, 100 ;  sleep for 100ms, +1
    call    waitdms  ; 5
    dec R17   ; +6
    brne waitcs_loop  ; +2 cycles on jump, +1 on ok

    ; +8 cycles
    pop R17 ; 4 = + 2 - 1
    ret      ; 8 (+ 4)

waitdms_long: ; argument in X(R27:R26) > 1 (10^-4)
    push    R17   ; 2
    push    R16 ; 4
waitdms_long_loop:
    ldi R16, 99    ; 1
    call    waitus     ; 5
    subi    XL, 1      ; 6
    sbci    XH, 0      ; 7
    mov     R17, XL    ; 8
    or      R17, XH    ; 9
    nop               ; 10
    brne    waitdms_long_loop ; 12
    pop R16   ; 5 (6-1)
    pop R17 ; 7
    ret         ; 11
