; This is a LCD library file

.EQU LCD_DATA_PORT = PORTD
.EQU LCD_E_PORT = PORTD
.EQU LCD_RS_PORT = PORTD
.EQU LCD_DATA_DDR = DDRD
.EQU LCD_E_DDR = DDRD
.EQU LCD_RS_DDR = DDRD
.EQU OE = PD1
.EQU RS = PD0
.EQU D4 = PD6 ; XXX: PD2 is used by INT0, TODO: use coherent pins
.EQU D5 = PD3
.EQU D6 = PD4
.EQU D7 = PD5

.EQU D_MASK = (1 << D4) | (1 << D5) | (1 << D6) | (1 << D7)

	
.EQU LCD_CLR_DISPLAY = 		0b00000001
.EQU LCD_RETURN_HOME = 		0b00000001
.EQU LCD_ENTRY_MODE_APPEND = 0b00000110
.EQU LCD_DISPLAY_CTR_DEF = 0b00001111
.EQU LCD_4BIT_TWO_LINE =   0b00101000

.EQU LCD_DISPLAY_NONE =   0b00001000
.EQU LCD_DISPLAY_LCD_ON = 0b00001100
.EQU LCD_DISPLAY_CURSOR = 0b00001010
.EQU LCD_DISPLAY_BLINK =  0b00001001
	

.EQU LCD_SHIFT_WINDOW_LEFT = 0b00011000
.EQU LCD_SHIFT_WINDOW_RIGHT = 0b00011100
.EQU LCD_SHIFT_CURSOR_LEFT = 0b00010000
.EQU LCD_SHIFT_CURSOR_RIGHT = 0b00010100

.EQU LCD_CMD_SET_CGRAM_ADDR = 0b01000000
.EQU LCD_CMD_SET_DDRAM_ADDR = 0b10000000


.MACRO LCD_RESEND
	sbi	LCD_E_PORT, OE
	nop
	nop
	nop
	nop
	cbi	LCD_E_PORT, OE
.ENDMACRO

; Macro-API
.MACRO LCD_SEND_INSTR
	ldi	    R16, @0
	call	lcd_send_instruction
.ENDMACRO

.MACRO LCD_SEND_DATA
	ldi	    R16, @0
	call	lcd_send_data
.ENDMACRO

.MACRO LCD_SET_DDRAM_ADDR
	ldi 	R16, LCD_CMD_SET_DDRAM_ADDR | @0
	call	lcd_send_instruction
	ldi	    R16, 80
	call	waitus
.ENDMACRO
; END Macro-Api
	
lcd_enter_4bit_mode:
	;; First phase
	cbi	LCD_E_PORT,  OE
	cbi   LCD_RS_PORT, RS
	
	SLEEP_DMS 400
	
	sbi	LCD_E_PORT,  OE
	sbi	LCD_DATA_PORT, D4
	sbi	LCD_DATA_PORT, D5
	cbi	LCD_DATA_PORT, D6
	cbi	LCD_DATA_PORT, D7
	
	nop
	nop
	nop
	nop
	
	cbi	LCD_E_PORT,  OE ; execute
	
	SLEEP_DMS 41
	
	; Second phase


	LCD_RESEND
	SLEEP_US	100
	
	; Third phase
	
	LCD_RESEND
	SLEEP_US	100
	
	; Fourth phase - enable 4-bit mode
	sbi	LCD_E_PORT, OE
	cbi	LCD_DATA_PORT, D4
	nop
	nop
	nop
	cbi	LCD_E_PORT, OE	
	
	SLEEP_US	40
	
	ret
	
; sends lower halfbyte
lcd_set_halfbyte:
   sbi	LCD_E_PORT, OE

	cbi	LCD_DATA_PORT, D4
	cbi	LCD_DATA_PORT, D5
	cbi	LCD_DATA_PORT, D6
	cbi	LCD_DATA_PORT, D7
	
	sbrc	R16, 0
	sbi	LCD_DATA_PORT, D4
	sbrc	R16, 1
	sbi	LCD_DATA_PORT, D5
	sbrc	R16, 2
	sbi	LCD_DATA_PORT, D6
	sbrc	R16, 3
	sbi	LCD_DATA_PORT, D7


  	ret ; 4 cycles	
	
lcd_send_byte:
	; send bits 4-7
	swap	R16
	call 	lcd_set_halfbyte ; at least 4 cycles delay
    cbi 	LCD_E_PORT, OE ; execute
	
	; send bits 0-3
	swap	R16
	call 	lcd_set_halfbyte  ; delayed > 4 cycles
    cbi     LCD_E_PORT, OE ; execute
	
	SLEEP_US	40 ; destroys R16
	ret
	

;;;;;;;;;;
;   API
;;;;;;;;;;

lcd_send_instruction:
	cbi	LCD_RS_PORT, RS
	rjmp	lcd_send_byte
	
lcd_send_data:
	sbi	LCD_RS_PORT, RS
	rjmp	lcd_send_byte

lcd_init:
	cbi	LCD_DATA_PORT, D4
	cbi	LCD_DATA_PORT, D5
	cbi	LCD_DATA_PORT, D6
	cbi	LCD_DATA_PORT, D7
	cbi	LCD_E_PORT,  OE
	cbi   LCD_RS_PORT, RS

	sbi	LCD_DATA_DDR, D4
	sbi	LCD_DATA_DDR, D5
	sbi	LCD_DATA_DDR, D6
	sbi	LCD_DATA_DDR, D7
	sbi	LCD_E_DDR,  OE
	sbi   LCD_RS_DDR, RS
	
	call lcd_enter_4bit_mode

 	LCD_SEND_INSTR	LCD_4BIT_TWO_LINE
    SLEEP_US		40
	LCD_SEND_INSTR	LCD_CLR_DISPLAY
	SLEEP_DMS 	    17
	LCD_SEND_INSTR	LCD_RETURN_HOME
	SLEEP_DMS 	    17
	LCD_SEND_INSTR	LCD_ENTRY_MODE_APPEND
	SLEEP_US		40
	LCD_SEND_INSTR	LCD_DISPLAY_CTR_DEF
	SLEEP_US		40
	ret
